<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $page ?> | Brand</title>
	<meta name="viewport" content="width=device-width" />
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
</head>
<body>
	<header>
		<div class="logo"><a href="#">Logo</a></div>
		<div id="mobile-btn"><a href="#">Menu</a></div>
		<nav>
			<ul>
				<li><span class="top"><i class="fa fa-home" aria-hidden="true"></i></span><a href="index.php">Home</a></li>
				<li><span class="top"><i class="fa fa-gbp" aria-hidden="true"></i></span><a href="#">Fund &amp; Support</a></li>
				<li><span class="top"><i class="fa fa-lightbulb-o" aria-hidden="true"></i></span><a href="#">Start Project</a></li>
				<li><span class="top"><i class="fa fa-question" aria-hidden="true"></i></span><a href="#">How It Works</a></li>
				<li id="log-reg"><span class="top"><input type="search" placeholder="Search"></span><a href="index.php">Login</a><a href="register.php">Register</a></li>
				<li><span class="top"><i class="fa fa-search" aria-hidden="true"></i></span><a href="#" ><p class="search-text">Search</p></a></li>
			</ul>
		</nav>

	</header>

	<div class="container">
		
