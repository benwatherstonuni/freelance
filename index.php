<?php 
$page = "Home";
@include('inc/header.php')
?>

<div class="half">	
	<div class="border">
		<h2>Quick Sign In</h2>
		<ul class="sign-in">
			<li><i class="fa fa-facebook" aria-hidden="true"></i></li>
			<li><i class="fa fa-twitter" aria-hidden="true"></i></li>
			<li><i class="fa fa-linkedin" aria-hidden="true"></i></li>
			<li><i class="fa fa-google" aria-hidden="true"></i></li>
			<li><i class="fa fa-google-plus" aria-hidden="true"></i></li>
			<li><i class="fa fa-yahoo" aria-hidden="true"></i></li>
			<li><i class="fa fa-openid" aria-hidden="true"></i></li>
			<li><i class="fa fa-angellist" aria-hidden="true"></i></li>
		</ul>
	</div>
</div>

<div class="half">
	
	<div class="border">
		<h2>Login</h2>
		<input type="text" placeholder="Username" class="username">
		<input type="text" placeholder="Password">
		<div class="row">
			<input type="checkbox" name="remember" >
			<label for="remember">Remember me</label>
		</div>
		<input type="submit" value="LOGIN" class="btn">
		<div class="forgot">
			<a href="#">Forgot your password?</a>
		</div>
	</div>
</div>

<?php @include('inc/footer.php') ?>

