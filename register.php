<?php 
$page = "Register";
@include('inc/header.php')
?>

<div class="half">	
	<div class="border">
		<h2>Quick Sign Up</h2>
		<ul class="sign-in">
			<li><i class="fa fa-facebook" aria-hidden="true"></i></li>
			<li><i class="fa fa-twitter" aria-hidden="true"></i></li>
			<li><i class="fa fa-linkedin" aria-hidden="true"></i></li>
			<li><i class="fa fa-google" aria-hidden="true"></i></li>
			<li><i class="fa fa-google-plus" aria-hidden="true"></i></li>
			<li><i class="fa fa-yahoo" aria-hidden="true"></i></li>
			<li><i class="fa fa-openid" aria-hidden="true"></i></li>
			<li><i class="fa fa-angellist" aria-hidden="true"></i></li>
		</ul>
	</div>
</div>

<div class="half">
	
	<div class="border" id="register">
		<h2>Register</h2>
		<input type="text" placeholder="Username" class="username">
		<input type="password" placeholder="Password">
		<input type="email" placeholder="Email">

		<img src="#" alt="Security Image">
		<input type="text" placeholder="Security Code">

		<select name="security-question" id="security-question">
			<option value="1">Mothers Maiden Name</option>
			<option value="2">First Car</option>
			<option value="3">Favorite Pet</option>
			<option value="4">Street you grew up in</option>
		</select>

		<input type="text" placeholder="Security Answer">
		
		<div class="forgot">
			<a href="#">By clicking register you are agreeing to the <span>Terms and Condidtions</span></a>
		</div>
		<input type="submit" value="LOGIN" class="btn">
	</div>
</div>

<?php @include('inc/footer.php') ?>

