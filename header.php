<?php 
$page = "Header";
@include('inc/header.php')
?>

	<div class="banner">
		<img src="images/header.jpg" alt="">
	</div>

	<div class="border dark top">

		<div class="company-container">
			
			<div class="company">
				<img src="#" alt="Company Picture">
			</div>

			<div class="company-info">
				<h2>Fashion Tailoring</h2>
				<ul>
					<li>A <span>Design</span> project in  <i class="fa fa-map-marker" aria-hidden="true"></i> Kanchipuram, India.</li>
					<li><i class="fa fa-envelope-o" aria-hidden="true"></i> Message</li>
				</ul>
			</div>
			
		</div>

		<div class="investors">
			<p>Investors</p>
			<ul>
				<li><img src="images/investor-1.jpg" alt="Investor 1"></li>
				<li><img src="images/investor-2.jpg" alt="Investor 2"></li>
				<li><img src="images/investor-3.jpg" alt="Investor 3"></li>
				<li><img src="images/investor-4.jpg" alt="Investor 4"></li>
			</ul>
		</div>

		<div class="message">
			<a href="#" class="btn clear">Follow</a>
		</div>

		<div class="message">
			<a href="#" class="btn">Invest</a>
		</div>

	</div>


<?php @include('inc/footer.php') ?>

